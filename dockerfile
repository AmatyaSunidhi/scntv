#API_BUILD
FROM node:12.22.6-buster as node_build
WORKDIR /app
COPY package*.json /app/
RUN npm install
COPY ./client/package*.json /app/client/
RUN cd client && \
    npm install
COPY . /app/

RUN cd client && \
    npm run build && \
    cd /app/ && \
    mkdir client_build && \
    mv client/dist client_build/ && \
    rm -rf client 

RUN mkdir -p server_build/dist && \
    npm run build && \
    npm prune --production=true && \
    mv node_modules server_build/ && \
    mv server server_build/ && \
    mv entrypoint.sh server_build/ && \
    mv .env server_build/

FROM node:12.22.6-buster-slim
LABEL maintainer="vikrum669@gmail.com"

RUN apt update && \
    apt install dumb-init

WORKDIR /app
#copy server build
COPY --from=node_build /app/server_build/ /app/
COPY --from=node_build /app/client_build/dist /app/client/dist

RUN chmod 755 entrypoint.sh

EXPOSE 5000
ENTRYPOINT [ "/app/entrypoint.sh"]

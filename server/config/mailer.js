import nodemailer from "nodemailer";
import path from "path";
import fs from "fs";
import Handlebars from "hbs";

const appDir = path.dirname(require.main.filename);
const templatesDir = path.join(appDir, "../server/emails");

/*const smtpTransport = nodemailer.createTransport({
    host: process.env.MAIL_HOST,
    service: process.env.MAIL_SERVER,
    port: process.env.MAIL_PORT,
    secure: true,
    auth: {
        user: process.env.MAIL_USER,
        pass: process.env.MAIL_PASSWORD,
    },
    tls: {
        rejectUnauthorized: false,
    },
});*/

// create template based sender function
const sendMail = (template, to, subject, locals, attachments) => {
    const templateContent = fs.readFileSync(
        `${templatesDir}/baseEmailTemplate.hbs`,
        "utf-8"
    );

    const compiledTemplate = Handlebars.compile(templateContent);
    const mailOptions = {
        from: process.env.MAIL_FROM,
        to,
        subject,
        html: compiledTemplate(locals),
        attachments: attachments || [],
    };

    const smtpDynamicTransport = nodemailer.createTransport({
        host: process.env.MAIL_HOST,
        service: process.env.MAIL_SERVER,
        port: process.env.MAIL_PORT,
        secure: true,
        auth: {
            user: process.env.MAIL_USER,
            pass: process.env.MAIL_PASSWORD,
        },
        tls: {
            rejectUnauthorized: false,
        },
    });

    return new Promise(function (resolve, reject) {
        smtpDynamicTransport.sendMail(mailOptions, function (error, response) {
            if (error) {
                console.log("Sending notification mail error :::", error);
                reject(error);
            } else {
                console.log(response, "Notification Email sent successfully");
                resolve(response);
            }
        })
    });
};

export default sendMail;

const EmailTemplateContent = {
    contactEmail: {
        name: "contactEmail",
        subject: "Enquiry form submission",
        content: "<p class=\"ql-align-center\"><strong>Hello Admin,</strong></p><p><br></p><p class=\"ql-align-center\">We have received the form submission by our audiences with the following details.</p><p><br></p><p>Name: {{name}}</p><p>Email: {{email}}</p><p>Contact Number: {{contactNumber}}</p><p>Address: {{address}}</p><p>City: {{city}}</p><p>District: {{district}}</p><p>Internet Provider: {{internetProvider}}</p><p>Tv Provider: {{tvProvider}}</p><p><br></p><p><br></p><p class=\"ql-align-center\">Thank you,</p><p class=\"ql-align-center\">The {{projectName}} Team</p><p class=\"ql-align-center\">@ 2021 {{projectName}}</p>"
    },
    contactSectionEmail: {
        name: "contactSectionEmail",
        subject: "Enquiry form submission",
        content: "<p class=\"ql-align-center\"><strong>Hello Admin,</strong></p><p><br></p><p class=\"ql-align-center\">We have received the enquiry form submission by our audiences with the following details.</p><p><br></p><p>Name: {{name}}</p><p>Email: {{email}}</p><p>Message: {{message}}</p><p><br></p><p><br></p><p class=\"ql-align-center\">Thank you,</p><p class=\"ql-align-center\">The {{projectName}} Team</p><p class=\"ql-align-center\">@ 2021 {{projectName}}</p>"
    },
}
export { EmailTemplateContent };

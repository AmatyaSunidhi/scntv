import httpStatusCode from "http-status-codes";
import responseMessages from "./constant.js";

const GENERAL = {
    SUCCESS: {
        title: "Success",
        statusCode: httpStatusCode.OK,
        message: responseMessages.EN.SUCCESS,
    },
    FAILURE: {
        title: "Failed",
        statusCode: httpStatusCode.INTERNAL_SERVER_ERROR,
        message: responseMessages.EN.SOMETHING_WENT_WRONG,
    },
    BAD_REQUEST: {
        title: "Bad Request",
        statusCode: httpStatusCode.BAD_REQUEST,
        message: responseMessages.EN.BAD_REQUEST,
    },
    EXCEPTION: {
        title: "exception",
        statusCode: httpStatusCode.INTERNAL_SERVER_ERROR,
        message: responseMessages.EN.SOMETHING_WENT_WRONG,
    },
    USER_NOT_FOUND: {
        title: "Error",
        statusCode: httpStatusCode.NOT_FOUND,
        message: responseMessages.EN.DB_USER_NOT_FOUND,
    },
    EMAIL_ALREADY_EXISTS: {
        title: responseMessages.EN.REGISTRATION_TITLE,
        statusCode: httpStatusCode.UNPROCESSABLE_ENTITY,
        status: false,
        message: responseMessages.EN.EMAIL_ALREADY_TAKEN,
    },
    UNREGISTERED_EMAIL: {
        title: "Login Failed",
        statusCode: httpStatusCode.UNAUTHORIZED,
        status: false,
        message: responseMessages.EN.UNREGISTERED_EMAIL,
    },
    PARTNER_ALREADY_EXISTS: {
        title: responseMessages.EN.PARTNER_TITLE,
        statusCode: httpStatusCode.UNPROCESSABLE_ENTITY,
        status: false,
        message: responseMessages.EN.PARTNER_ALREADY_EXIST,
    },
    LOGIN_FAILURE: {
        title: "Login Failed",
        statusCode: httpStatusCode.UNAUTHORIZED,
        status: false,
        message: responseMessages.EN.CREDENTIAL_FAILED,
    },
    LOGIN_SUCCESS: {
        title:"Login",
        statusCode: httpStatusCode.OK,
        status: true,
        message: responseMessages.EN.LOGIN_SUCCESS,
    },
    FORGOT_PASSWORD: {
        FAILURE: {
            title: "Unsuccess",
            statusCode: httpStatusCode.UNPROCESSABLE_ENTITY,
            status: false,
            message: responseMessages.EN.USER_NOT_FOUND,
        },
        SUCCESS: {
            title: "",
            statusCode: httpStatusCode.OK,
            status: true,
            message: responseMessages.EN.FORGOT_PASSWORD_MAIL_SENT,
        },
        FAILURE_WITH_UNVERIFIED_EMAIL: {
            title: "",
            statusCode: httpStatusCode.MOVED_TEMPORARILY,
            status: false,
            message: responseMessages.EN.ACCOUNT_NOT_VERIFIED,
        },
    },
    RESET_PASSWORD: {
        INVALID_TOKEN: {
            title: "",
            statusCode: httpStatusCode.PRECONDITION_FAILED,
            message: responseMessages.EN.INVALID_TOKEN,
        },
        FAILURE_WITH_EXPECTION: {
            title: "",
            statusCode: httpStatusCode.EXPECTATION_FAILED,
            status: false,
            message: responseMessages.EN.EXPIRED_FORGOT_PASSWORD_TOKEN,
        },
        SUCCESS: {
            title: "",
            statusCode: httpStatusCode.OK,
            status: true,
            message: responseMessages.EN.RESET_PASSWORD_SUCCESS,
        },
        USER_NOT_FOUND: {
            title: "",
            statusCode: httpStatusCode.NOT_FOUND,
            status: false,
            message: responseMessages.EN.DB_USER_NOT_FOUND,
        },
    },
};

export { GENERAL };

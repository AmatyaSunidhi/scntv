const responseMessages = {
    EN: {
        UNKNOWN_ERROR: "Unknown error.",
        SUCCESS: "Success",
        CREDENTIAL_FAILED: "Email/Password is invalid. Please try again.",
        ADMIN_APPROVED: "Your account is not approved by admin",
        ACCOUNT_NOT_VERIFIED:
            "An account for this email exists but isn`t verified. Would you like us to resend the verification mail?",
        SOMETHING_WENT_WRONG: "Something went wrong. Please try again later.",
        SOMETHING_WENT_WRONG_WITH_DB_CONNECTION:
            "Something went wrong with db connection. Please try again later.",
        BAD_REQUEST:
            "Something went wrong with requested url. Please try again later.",
        LOGIN_SUCCESS: "Logged in successfully",
        EMAIL_ALREADY_TAKEN: "Email is already in use.",
        EMAIL_NOT_TAKEN: "Email is not in use.",
        VALIDATION_ERROR: "Validation Errors.",
        VALIDATION_SUCCESS: "Validation Success.",
        VALIDATION_FAILURE: "Validation failed.",
        NOT_ALLOWED: "Not allowed to update this data",
        USER_NOT_FOUND:
            "We are unable to find any account associated with this email.",
        DB_USER_NOT_FOUND: "We are unable to find any account.",
        DB_CLINIC_NOT_FOUND: "We are unable to find any clinic.",
        ALREADY_VERIFIED: "User already activated",
        VERIFICATION_MAIL_SENT: "Email sent successfully. Please check your inbox.",
        FORGOT_PASSWORD_MAIL_SENT:
            "Password reset link sent successfully. Please check your inbox.",
        REGISTRATION_SUCCESS:
            "Registration successful. Check your inbox for updates.",
        EXPIRED_FORGOT_PASSWORD_TOKEN: "Invalid/Expired forgot password token",
        RESET_PASSWORD_SUCCESS: "Password changed successfully",
        ACCOUNT_VERIFIED_SUCCESS: "Account activated successfully",
        CHANGE_PASSWORD: "Password changed successfully.",
        UNREGISTERED_EMAIL:
            "Email is not registered. Please try again with a valid email",

        // Pages
        PAGE_SUCCESS: "Page found",
        PAGE_ERROR: "Page not found",
        PAGE_ERROR_EXCEPTION: "Page cannot be found due to exception",

        /* -------------------titles list----------------*/
        REGISTRATION_TITLE: "Registration",
        PARTNER_TITLE: "Partner",
        PARTNER_ALREADY_EXIST: "Company name already exist for partner"
    },
};

export default responseMessages;

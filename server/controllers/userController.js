import * as httpStatusCode from "http-status-codes";
import {
    respondError,
    respondSuccess
} from "../helpers/responseHelper";
import { GENERAL } from "../constants/generalConstant";
import {sendContactEmail, sendContactSectionEmail} from "../helpers/customerHelper";


export const sendContactEmailToAdmin = async (req, res) => {
    const data = req.body;

    const result = await sendContactEmail(data);
    if(result === false) {
        return respondError(
            res,
            GENERAL.FAILURE.statusCode,
            GENERAL.FAILURE.title,
            "Failed to send email"
        )
    } else {
        return respondSuccess(
            res,
            httpStatusCode.OK,
            GENERAL.SUCCESS.title,
            GENERAL.SUCCESS.message
        )
    }
};

export const sendContactSectionEmailToAdmin = async (req, res) => {
    const data = req.body;

    const result = await sendContactSectionEmail(data);
    if(result === false) {
        return respondError(
            res,
            GENERAL.FAILURE.statusCode,
            GENERAL.FAILURE.title,
            "Failed to submit message"
        )
    } else {
        return respondSuccess(
            res,
            httpStatusCode.OK,
            GENERAL.SUCCESS.title,
            GENERAL.SUCCESS.message
        )
    }
};

import express from "express";
import generalRouter from "./generals";

const version = process.env.API_VERSION || 1;
const router = express.Router();

router.use(`/api/v${version}/general`, generalRouter);

export default router;

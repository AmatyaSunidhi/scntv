import express from "express";
import {createUser, getUserList} from "../controllers/userController";

const router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.get('/getUserList', getUserList );

router.post('/createUser', createUser);

module.exports = router;

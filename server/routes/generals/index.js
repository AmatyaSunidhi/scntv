import express from "express";
import {sendContactEmailToAdmin, sendContactSectionEmailToAdmin} from "../../controllers/userController";

const router = express.Router();

router.post('/sendContactEmail', sendContactEmailToAdmin);

router.post('/sendContactSectionEmail', sendContactSectionEmailToAdmin);

export default router;

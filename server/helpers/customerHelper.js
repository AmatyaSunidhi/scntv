import sendMail from "../config/mailer";
import { EmailTemplateContent } from "../emails/EmailContent";
import { isError } from "@hapi/joi";

export const parseContent = async (emailContent, emailType = false) => {
    const matches = emailContent.content.match(/{{(.+?)}}/g);

    if (matches) {
        matches.forEach((item) => {
            if (item === "{{name}}") {
                emailContent.content = emailContent.content.replace(
                    "{{name}}",
                    emailContent.name || ""
                );
            }
            if (item === "{{email}}") {
                emailContent.content = emailContent.content.replace(
                    "{{email}}",
                    emailContent.email || ""
                );
            }
            if (item === "{{contactNumber}}") {
                emailContent.content = emailContent.content.replace(
                    "{{contactNumber}}",
                    emailContent.contactNumber || ""
                );
            }
            if (item === "{{address}}") {
                emailContent.content = emailContent.content.replace(
                    "{{address}}",
                    emailContent.address || ""
                );
            }
            if (item === "{{city}}") {
                emailContent.content = emailContent.content.replace(
                    "{{city}}",
                    emailContent.city || ""
                );
            }
            if (item === "{{district}}") {
                emailContent.content = emailContent.content.replace(
                    "{{district}}",
                    emailContent.district || ""
                );
            }
            if (item === "{{internetProvider}}") {
                emailContent.content = emailContent.content.replace(
                    "{{internetProvider}}",
                    emailContent.internetProvider || ""
                );
            }
            if (item === "{{tvProvider}}") {
                emailContent.content = emailContent.content.replace(
                    "{{tvProvider}}",
                    emailContent.tvProvider || ""
                );
            }
            if (item === "{{message}}") {
                emailContent.content = emailContent.content.replace(
                    "{{message}}",
                    emailContent.message || ""
                );
            }
            if (item === "{{projectName}}") {
                emailContent.content = emailContent.content.replace(
                    "{{projectName}}",
                    process.env.APP_NAME || ""
                );
            }

            if (item === "{{content}}") {
                emailContent.content = emailContent.content.replace(
                    "{{content}}",
                    emailContent.content || ""
                );
            }

            if (item === "{{link}}") {
                let buttonText = "";
                if (emailType === "notificationEmail") {
                    buttonText = "Click Here";
                }

                emailContent.content = emailContent.content.replace(
                    "{{link}}",
                    `${"<a style='cursor: pointer;' href="}${emailContent.link
                    }><button style='background-color: cornflowerblue;
                  padding: 15px 20px;
                  border: none;
                  font-weight: 600;
                  font-size: 15px;
                  border-radius: 5px;
                  text-align: center;
                  text-decoration: none;
                  color: white;
                  display: inline-block;
                  margin: 4px 2px;
                  cursor: pointer;'>Click here</button></a>
                  <p style='display: block; margin-top: 10px !important;'>
                  <a style='cursor: pointer; 
                  color: cornflowerblue;
                  font-weight: 600;
                  text-decoration: none;' 
                  href="${emailContent.link
                    }">Click here</a> if the button is not working.</p>`
                );
            }
        })
    }
    return emailContent.content;
};


export const sendContactEmail = async (userDetails, task = null) => {
    const template = EmailTemplateContent.contactEmail;
    const receiverEmail = process.env.TEAM_EMAIL;

    const emailContent = {};
    emailContent.content = template.content;
    emailContent.name = userDetails.name;
    emailContent.email = userDetails.email;
    emailContent.contactNumber = userDetails.contactNumber;
    emailContent.address = userDetails.address;
    emailContent.city = userDetails.city;
    emailContent.district = userDetails.district;
    emailContent.internetProvider = userDetails.internetProvider;
    emailContent.tvProvider = userDetails.tvProvider;
    emailContent.projectName = process.env.APP_NAME;

    const content = await parseContent(emailContent, "contactEmail");

    const locals = {
        name: userDetails.name,
        email: userDetails.email,
        contactNumber: userDetails.contactNumber,
        address: userDetails.address,
        city: userDetails.city,
        district: userDetails.district,
        internetProvider: userDetails.internetProvider,
        tvProvider: userDetails.tvProvider,
        projectName: process.env.APP_NAME,
        contactEmail: "no-reply@scntv.com.np",
        content,
    };

    let subject = template.subject;
    let output = true;
    await sendMail("contactEmail", receiverEmail, subject, locals)
        .then(res => {
            output = true;
        }).catch(err => {
            output = false;
        });

    return output;

};

export const sendContactSectionEmail = async (userDetails, task = null) => {
    const template = EmailTemplateContent.contactSectionEmail;
    const receiverEmail = process.env.TEAM_EMAIL;

    const emailContent = {};
    emailContent.content = template.content;
    emailContent.name = userDetails.name;
    emailContent.email = userDetails.email;
    emailContent.message = userDetails.message;
    emailContent.projectName = process.env.APP_NAME;

    const content = await parseContent(emailContent, "contactSectionEmail");

    const locals = {
        name: userDetails.name,
        email: userDetails.email,
        message: userDetails.message,
        projectName: process.env.APP_NAME,
        contactEmail: "no-reply@scntv.com.np",
        content,
    };

    let subject = template.subject;
    let output = true;
    await sendMail("contactSectionEmail", receiverEmail, subject, locals)
        .then(res => {
            output = true;
        }).catch(err => {
            output = false;
        });

    return output;

};

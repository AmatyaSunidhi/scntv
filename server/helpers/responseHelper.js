export const respondSuccess = async (res, statusCode, title, message, data = []) => {
    res.status(statusCode).send({
        title,
        message,
        data : data,
    });
}

export const respondError = async (res, statusCode, title, message) => res.status(statusCode).send({
    title,
    message
});

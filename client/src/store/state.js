const userInfoLocalStorage =
  JSON.parse(
    localStorage.getItem(`${process.env.VUE_APP_ACCESS_TOKEN_NAME}User`)
  ) || {};

// *From Auth - Data will be received from auth provider
const userDefaults = {
  uEmail: userInfoLocalStorage.email, // From Auth
  displayName:
    userInfoLocalStorage.firstName + " " + userInfoLocalStorage.lastName, // From Auth
  userType: userInfoLocalStorage.userType
};

// Set default values for active-user
// More data can be added by auth provider or other plugins/packages
const getUserInfo = () => {
  let userInfo = {};

  // Update property in user
  Object.keys(userDefaults).forEach(key => {
    // If property is defined in localStorage => Use that
    userInfo[key] = userInfoLocalStorage[key]
      ? userInfoLocalStorage[key]
      : userDefaults[key];
  });

  // Include properties from localStorage
  Object.keys(userInfoLocalStorage).forEach(key => {
    if (userInfo[key] == undefined && userInfoLocalStorage[key] != null)
      userInfo[key] = userInfoLocalStorage[key];
  });

  return userInfo;
};

// /////////////////////////////////////////////
// State
// /////////////////////////////////////////////

const state = {
  AppActiveUser: getUserInfo()
};

export default state;

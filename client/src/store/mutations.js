
const mutations = {
  // Updates user info in state and localstorage
  UPDATE_USER_INFO(state, payload) {
    // Get Data localStorage
    let userInfo =
      JSON.parse(
        localStorage.getItem(`${process.env.VUE_APP_ACCESS_TOKEN_NAME}User`)
      ) || state.AppActiveUser;

        if (payload) {
      payload.uid = payload._id;
      payload.displayName = payload.firstName + " " + payload.lastName;
      payload.userRole = payload.userType;

      state.AppActiveUser = payload;

      userInfo.badge = payload.badge;
      userInfo.badgeCount = payload.badge;
      state.badgeCount = payload.badge;
    }

    // for (const property of Object.keys(payload)) {
    //   if (payload[property] != null) {
    //     // If some of user property is null - user default property defined in state.AppActiveUser
    //     state.AppActiveUser = payload[property];

    //     // Update key in localStorage
    //     userInfo[property] = payload[property];
    //   }
    // }
    // Store data in localStorage
    localStorage.setItem(
      `${process.env.VUE_APP_ACCESS_TOKEN_NAME}User`,
      JSON.stringify(userInfo)
    );
  },

  UPDATE_USER_TEMP_PROFILE_INFO(state, payload) {
    if (payload) {
      state.AppActiveUser.tempProfile = payload.tempProfile;
    }
  },
  // Updates user info in state and localstorage
  UPDATE_USER_BADGE_COUNT(state, payload) {
    // Get Data localStorage
    let userInfo =
      JSON.parse(
        localStorage.getItem(`${process.env.VUE_APP_ACCESS_TOKEN_NAME}User`)
      ) || state.AppActiveUser;

    if (payload && state.AppActiveUser) {
      state.AppActiveUser.badge = payload.badge;
      userInfo.badge = payload.badge;
      userInfo.badgeCount = payload.badge;
      state.badgeCount = payload.badge;
    }

    localStorage.setItem(
      `${process.env.VUE_APP_ACCESS_TOKEN_NAME}User`,
      JSON.stringify(userInfo)
    );
  },
 

  // update user permissions on run time
  UPDATE_USER_PERMISSION(state, payload) {
    if (payload) state.AppActiveUser.permissions = payload;
  }
};

export default mutations;

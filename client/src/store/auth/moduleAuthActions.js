import jwt from "../../http/jwt/index";
import router from "../../routes/router";
import httpStatusCode from "http-status-codes";
import axios from "@/axios";
import Vue from "vue";
import VueCookies from "vue-cookies";
Vue.use(VueCookies);

export default {
  // Admin login  JWT
  loginAdminJWT({ commit }, payload) {
    return new Promise((resolve, reject) => {
      jwt
        .loginAdmin(
          payload.userDetails.email,
          payload.userDetails.password,
        )
        .then(response => {
          const user = response.data.data.user;
          // If there's user data in response
          if (response.data) {
            localStorage.setItem(
              `${process.env.VUE_APP_ACCESS_TOKEN_NAME}FirstName`,
              response.data.data.user.firstName
            );
            localStorage.setItem(
              `${process.env.VUE_APP_ACCESS_TOKEN_NAME}User`,
              JSON.stringify(response.data.data.user)
            );

            // Set accessToken and refresh token
            localStorage.setItem(
              `${process.env.VUE_APP_ACCESS_TOKEN_NAME}AccessToken`,
              response.data.data.token.accessToken
            );

            localStorage.setItem(
              `${process.env.VUE_APP_ACCESS_TOKEN_NAME}RefreshToken`,
              response.data.data.token.refreshToken
            );

            localStorage.setItem(
              `${process.env.VUE_APP_ACCESS_TOKEN_NAME}LoggedIn`,
              "true"
            );

            // Update user details
            commit("UPDATE_USER_INFO", response.data.data.user, { root: true });

            // Set bearer token in axios
            commit("SET_BEARER", response.data.data.token.accessToken);

            // router.push("/admin/dashboard");
            console.log("usertype", user.userType)
            if (user.userType === "admin") {
              router.push("/admin/dashboard");
            }
            if (user.userType === "startup") {
              router.push("/startup/dashboard");
            }
            if (user.userType === "investor") {
              router.push("/investor/dashboard");
            }
          }
        })
        .catch(response => {
          reject(response);
          /* if (
            response &&
            response.message &&
            response.message === "Request failed with status code 401"
          )
            reject({
              message: "Email/Password is invalid. Please try again."
            });
          else if (!response.data) {
            reject({
              message: response
            });
          } else reject({ message: response.data.message }); */
        });
    });
  },
  registerUserJWT({ commit }, payload) {
    const {
      displayName,
      email,
      password,
      confirmPassword,
      contactNumber,
      userType
    } = payload.userDetails;

    return new Promise((resolve, reject) => {
      // Check confirm password
      if (password !== confirmPassword) {
        reject({ message: "Password doesn't match. Please try again." });
      }
      if (userType === "franchise") {
        jwt
          .registerFranchise(payload.userDetails)
          .then(response => {
            // Redirect User
            router.push(router.currentRoute.query.to || "/");

            // Update data in localStorage
            localStorage.setItem("accessToken", response.data.accessToken);
            commit("UPDATE_USER_INFO", response.data.userData, { root: true });

            resolve(response);
          })
          .catch(error => {
            // console.log(error.response)
            // console.log(response.data)
            reject(error);
          });
      } else {
        jwt
          .registerUser(displayName, email, password, userType)
          .then(response => {
            // Redirect User
            router.push(router.currentRoute.query.to || "/");

            // Update data in localStorage
            localStorage.setItem("accessToken", response.data.accessToken);
            commit("UPDATE_USER_INFO", response.data.userData, { root: true });

            resolve(response);
          })
          .catch(error => {
            reject(error);
          });
      }
    });
  },
  fetchAccessToken() {
    return new Promise(resolve => {
      jwt.refreshToken().then(response => {
        resolve(response);
      });
    });
  },
  resetPassword({ commit }, payload) {
    return new Promise((resolve, reject) => {
      jwt
        .resetPassword(payload.userDetails.password, payload.userDetails.token)
        .then(response => {
          if (response.status == httpStatusCode.OK) {
            resolve(response);
          } else {
            reject({ message: response.data.message });
          }
        })
        .catch(response => {
          reject({ message: response.data.message });
        });
    });
  },
  forgotPwd({ commit }, payload) {
    return new Promise((resolve, reject) => {
      jwt
        .forgotPasswordAdmin(payload.userDetail.email)
        .then(response => {
          if (response.data) {
            resolve(response);
          } else {
            reject({ message: response.data.message });
          }
        })
        .catch(response => {
          reject({ message: response.data.message });
        });
    });
  },
  changeUserPassword({ commit }, payload) {
    return new Promise((resolve, reject) => {
      jwt
        .changePassword(
          payload.userDetails.currentPassword,
          payload.userDetails.newPassword
        )
        .then(response => {
          // If there's user data in response
          if (response.status == httpStatusCode.OK) {
            resolve(response);
          } else {
            reject({ message: response.data.message });
          }
        })
        .catch(err => {
          reject({ message: err.data.message });
        });
    });
  },

  checkTokenExpiry({ commit }, payload) {
    return new Promise((resolve, reject) => {
      jwt
        .checkTokenExpiry(payload.token)
        .then(response => {
          // If there's user data in response
          if (response.status == httpStatusCode.OK) {
            resolve(response);
          } else {
            reject({ message: response.data.message });
          }
        })
        .catch(({ response }) => {
          reject({ message: response.data.message });
        });
    });
  },

  verifyAccount({ commit }, payload) {
    return new Promise((resolve, reject) => {
      axios
        .post("/api/v1/user/verifyAccount", payload)
        .then(res => {
          return resolve(res);
        })
        .catch(err => {
          return reject(err.response);
        });
    });
  },

  checkLoggedIn({ commit }, payload) {
    if (payload.token) {
      if (payload.userType === "superAdmin") {
        return "/admin/dashboard";
      }
      if (payload.userType === "franchise") {
        router.push("/franchise/dashboard");
      }
      if (payload.userType === "clinic") {
        router.push("/clinic/dashboard");
      }
      if (payload.userType === "doctor") {
        router.push("/doctor/dashboard");
      }
      if (payload.userType === "nurse") {
        router.push("/nurse/dashboard");
      }
      if (payload.userType === "admin") {
        router.push("/sub-admin/dashboard");
      }
    }
  }
};

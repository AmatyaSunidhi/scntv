import jwt from "../../http/jwt/index";
import router from "../../routes/router";
import httpStatusCode from "http-status-codes";
import axios from "@/axios";
import Vue from "vue";
import VueCookies from "vue-cookies";
Vue.use(VueCookies);

export default {

  // Notify admin about contact form
  sendContactEmail({ commit }, data) {
    return new Promise((resolve, reject) => {
      jwt
        .httpSendContactEmail(data)
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    })
  },
  sendContactSectionEmail({ commit }, data) {
    return new Promise((resolve, reject) => {
      jwt
        .httpSendContactSectionEmail(data)
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    })
  },

};

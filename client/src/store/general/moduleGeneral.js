import state from './moduleGeneralState.js'
import mutations from './moduleGeneralMutations.js'
import actions from './moduleGeneralActions.js'
import getters from './moduleGeneralGetters.js'

export default {
	namespaced: true,
    state: state,
    mutations: mutations,
    actions: actions,
    getters: getters
}

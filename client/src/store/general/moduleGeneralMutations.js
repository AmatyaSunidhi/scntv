import axios from "../../http/axios/index";

export default {
  SET_BEARER(state, accessToken) {
    console.log("access", accessToken);
    axios.defaults.headers.common['Authorization'] = accessToken
  }
}

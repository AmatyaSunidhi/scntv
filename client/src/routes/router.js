import Vue from 'vue';
import VueRouter from 'vue-router';
import routes from './routes';
import { auth } from '../auth/auth';


Vue.use(VueRouter);

// configure router
const router = new VueRouter({
  routes,
  mode: "history",
  base: process.env.BASE_URL,
  scrollBehavior: (to, from ,savedPosition) => {
    return { x: 0, y: 0 };
  }
});

router.beforeEach((to, from, next) => {
  const exceptRoute = [
    "/",
    "/landing",
    "/register",
    "/forgot-password",
    "/privacy-policy",
    "/terms-and-conditions",
    "/faq"
  ];

  if (!exceptRoute.includes(to.path)) {
    auth(to, from, next);
    return next();
  } else {
    return next();
  }
});

export default router;

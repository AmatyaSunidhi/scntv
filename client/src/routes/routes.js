import DashboardLayout from '@/views/Layout/DashboardLayout.vue';
import AuthLayout from '@/views/Pages/AuthLayout.vue';
import NotFound from '@/views/NotFoundPage.vue';

const routes = [
  { path: '*', component: NotFound },
  {
    path: '/',
    redirect: '/',
    component: AuthLayout,
    children: [
      {
        path: '/',
        name: 'landing-page',
        component: () => import('../views/Pages/LandingPage.vue'),
        meta: {
          rule: "public"
        }
      },
      {
        path: '/service',
        name: 'service',
        component: () => import('../views/Pages/Supporting Page/ServicePage.vue'),
        meta: {
          rule: "public"
        }
      },
      {
        path: '/company',
        name: 'company',
        component: () => import('../views/Pages/Supporting Page/CompanyPage.vue'),
        meta: {
          rule: "public"
        }
      },
      {
        path: '/career',
        name: 'career',
        component: () => import('../views/Pages/Supporting Page/CareerPage.vue'),
        meta: {
          rule: "public"
        }
      },
      {
        path: '/contact',
        name: 'contact',
        component: () => import('../views/Pages/Supporting Page/ContactPage.vue'),
        meta: {
          rule: "public"
        }
      },
      {
        path: '/blocked-content',
        name: 'blocked-content',
        component: () => import('../views/Pages/Supporting Page/BlockContentPage.vue'),
        meta: {
          rule: "public"
        }
      },
      {
        path: '/login',
        name: 'login',
        component: () => import('../views/Pages/Login.vue'),
        meta: {
          rule: "public"
        }
      },
      {
        path: '/register',
        name: 'register',
        component: () => import('../views/Pages/Register.vue')
      },
      {
        path: '/faq',
        name: 'Faq',
        component: () => import('../views/Pages/Faq.vue'),
        meta: {
          rule: "public"
        }
      },
      {
        path: '/privacy-policy',
        name: 'privacy-policy',
        component: () => import('../views/Pages/PrivacyPolicy.vue'),
        meta: {
          rule: "public"
        }
      },
      {
        path: '/terms-and-conditions',
        name: 'terms-and-conditions',
        component: () => import('../views/Pages/TermsAndConditions.vue'),
        meta: {
          rule: "public"
        }
      },
    ]
  },
 /* {
    path: '/',
    redirect: 'dashboard',
    component: DashboardLayout,
    children: [
      {
        path: '/dashboard',
        name: 'dashboard',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/!* webpackChunkName: "demo" *!/ '../views/Dashboard.vue')
      },
      {
        path: '/icons',
        name: 'icons',
        component: () => import(/!* webpackChunkName: "demo" *!/ '../views/Icons.vue')
      },
      {
        path: '/profile',
        name: 'profile',
        component: () => import(/!* webpackChunkName: "demo" *!/ '../views/Pages/UserProfile.vue')
      },
      {
        path: '/maps',
        name: 'maps',
        component: () => import(/!* webpackChunkName: "demo" *!/ '../views/GoogleMaps.vue')
      },
      {
        path: '/tables',
        name: 'tables',
        component: () => import(/!* webpackChunkName: "demo" *!/ '../views/RegularTables.vue')
      }
    ]
  },*/

];

export default routes;

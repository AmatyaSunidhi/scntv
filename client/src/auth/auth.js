export const auth = (to, from, next) => {
  const token = localStorage.getItem(
    `${process.env.VUE_APP_ACCESS_TOKEN_NAME}AccessToken`
  );
  const isLoggedIn = localStorage.getItem(
    `${process.env.VUE_APP_ACCESS_TOKEN_NAME}LoggedIn`
  );

  const publicPages = [
    "/landing",
    "/service",
    "/company",
    "/career",
    "/contact",
    "/blocked-content",
    "/login",
    "/register",
    "/forgot-password",
    "/privacy-policy",
    "/terms-and-conditions",
    "/faq"
  ];

  const authRequired = !publicPages.includes(to.path);

  if (!authRequired && isLoggedIn && token) {
    const user = JSON.parse(
      localStorage.getItem(`${process.env.VUE_APP_ACCESS_TOKEN_NAME}User`)
    );
    if (user.userType === "admin") {
      return next("/");
    }
  }

  if (authRequired && !isLoggedIn && !token) {
    if (from.path !== "/login") {
      return next("/login");
    }
  }

  /*  if(!authRequired && !isLoggedIn && !token) {
     if (from.path !== "/landing") {
       return next("/landing");
     }
   } */

  return next();
};

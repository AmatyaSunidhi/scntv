import axios from "@/axios";
import store from "../../store/store.js";
import router from "../../routes/router.js";

// Token Refresh
let isAlreadyFetchingAccessToken = false;

function onAccessTokenFetched(refreshData) {
  localStorage.setItem(
    `${process.env.VUE_APP_ACCESS_TOKEN_NAME}AccessToken`,
    refreshData.accessToken
  );
  localStorage.setItem(
    `${process.env.VUE_APP_ACCESS_TOKEN_NAME}FirstName`,
    refreshData.user.firstName
  );
  localStorage.setItem(
    `${process.env.VUE_APP_ACCESS_TOKEN_NAME}User`,
    JSON.stringify(refreshData.user)
  );
  setCookie("token", refreshData.accessToken, 1);
  Vue.$cookies.set(
    `${process.env.VUE_APP_ACCESS_TOKEN_NAME}RefreshToken`,
    refreshData.refreshToken,
    refreshData.refreshTokenExpiresIn,
    "",
    window.location.host,
    true
  );
  localStorage.setItem(
    `${process.env.VUE_APP_ACCESS_TOKEN_NAME}LoggedIn`,
    "true"
  );
  store.commit("UPDATE_USER_INFO", refreshData.user, { root: true });
  store.commit("SET_BEARER", refreshData.accessToken);
}

export default {
  init() {
    axios.interceptors.request.use(
      config => {
        const token = localStorage.getItem(
          `${process.env.VUE_APP_ACCESS_TOKEN_NAME}AccessToken`
        );
        if (token) {
          config.headers.Authorization = token;
        }
        return config;
      },
      error => {
        return Promise.reject(error);
      }
    );
    axios.interceptors.response.use(
      function (response) {
        return response;
      },
      function (error) {
        if (error && error.message && error.message === "Network Error") {
          return Promise.reject({
            data: { message: "Network Error. Couldn't connect to server." }
          });
        }

        const {
          config,
          response: { status }
        } = error;

        if (config.url.includes("login") && status === 401) {
          return Promise.reject(error);
        }

        if (config.url.includes("refreshToken") && status === 401) {
          localStorage.clear();
          router.push("/login");
        }

        if (config.url.includes("refreshToken") && status === 422) {
          localStorage.clear();
          router.push("/login");
        }

        const originalRequest = config;


        if (status === 401) {
          if (!isAlreadyFetchingAccessToken) {
            isAlreadyFetchingAccessToken = true;
            store
              .dispatch("auth/fetchAccessToken")
              .then(refreshTokenResponse => {
                isAlreadyFetchingAccessToken = false;
                onAccessTokenFetched(refreshTokenResponse.data.data);
                originalRequest.headers.Authorization =
                  "Bearer " + refreshTokenResponse.data.data.accessToken;
                bus.$vs.loading.close();
                return Promise.resolve(axios(originalRequest));
              })
              .catch(err => {
                bus.$vs.loading.close();
                return Promise.reject(axios(originalRequest));
              });
          }
        }

        if (status === 403 && error.response.data === "Permission Denied") {
          router.push("/admin/permission-denied");
        }

        return Promise.reject(error.response);
      }
    );
  },
  loginAdmin(email, pwd) {
    return axios.post("/api/v1/auth/login", {
      email: email,
      password: pwd
    });
  },

  httpSendContactEmail(data) {
    return axios.post("/api/v1/general/sendContactEmail", data)
  },

  httpSendContactSectionEmail(data) {
    return axios.post("/api/v1/general/sendContactSectionEmail", data)
  },

}

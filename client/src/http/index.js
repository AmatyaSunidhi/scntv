import jwt from "../../http/requests/auth/jwt/index.js";
import firebase from "firebase/app";
import "firebase/auth";
import router from "@/router";
import httpStatusCode from "http-status-codes";
import { use } from "vee-validate/dist/vee-validate.minimal.esm";
import axios from "@/axios";
import Vue from "vue";
import VueCookies from "vue-cookies";
Vue.use(VueCookies);

export default {
  loginJWT({commit}, payload) {
    return new Promise((resolve, reject) => {
      jwt
        .login(
          payload.userDetails.email,
          payload.userDetails.password,
        )
        .then(response => {
          // If there's user data in response
          if (response.data) {
            // router.push(router.currentRoute.query.to || '/')
            localStorage.setItem(
              `${process.env.VUE_APP_ACCESS_TOKEN_NAME}FirstName`,
              response.data.data.user.firstName
            );
            localStorage.setItem(
              `${process.env.VUE_APP_ACCESS_TOKEN_NAME}User`,
              JSON.stringify(response.data.data.user)
            );
            // Set accessToken and refresh token

            localStorage.setItem(
              `${process.env.VUE_APP_ACCESS_TOKEN_NAME}AccessToken`,
              response.data.data.token.accessToken
            );

            Vue.$cookies.set(
              `${process.env.VUE_APP_ACCESS_TOKEN_NAME}RefreshToken`,
              response.data.data.token.refreshToken,
              response.data.data.token.refreshTokenExpiresIn,
              "",
              window.location.host,
              true
            );

            localStorage.setItem(
              `${process.env.VUE_APP_ACCESS_TOKEN_NAME}LoggedIn`,
              "true"
            );

            // Update user details
            commit("UPDATE_USER_INFO", response.data.data.user, {root: true});

            // Set bearer token in axios
            commit("SET_BEARER", response.data.data.token.accessToken);

            if (response.data.data.user.userType === "superAdmin") {
              // Navigate User to homepage
              router.push("/super-admin/dashboard");
            }
            // if (
            //   (response.data.data.user.userType === "franchise" &&
            //     response.data.data.user.isProfileCompleted) ||
            //   response.data.data.user.userType === "subFranchise"
            // ) {
            //   router.push("/franchise/dashboard");
            // }
            // if (
            //   (response.data.data.user.userType === "clinic" &&
            //     response.data.data.user.isProfileCompleted) ||
            //   response.data.data.user.userType === "subClinic"
            // ) {
            //   if (
            //     response.data.data.user.userType === "clinic" &&
            //     (!response.data.data.user.financialDetails ||
            //       !response.data.data.user.financialDetails.accountNumber)
            //   ) {
            //     router.push("/clinic/setup-bank-detail");
            //   } else {
            //     router.push("/clinic/dashboard");
            //   }
            // }

            if (response.data.data.user.userType === "clinic") {
              if (response.data.data.user.isProfileCompleted)
                router.push("/clinic/dashboard");
              else {
                router.push("/clinic-complete-profile");
              }
            }

            if (response.data.data.user.userType === "subClinic")
              router.push("/clinic/dashboard");

            if (
              response.data.data.user.userType === "franchise" ||
              response.data.data.user.userType === "subFranchise"
            ) {
              router.push("/franchise/dashboard");
            }

            // if (
            //   response.data.data.user.userType === "clinic" ||
            //   response.data.data.user.userType === "subClinic"
            // ) {
            //   if (
            //     response.data.data.user.userType === "clinic" && response.data.data.user.defaultPaymentMethod !=='CreditCard' &&
            //     (!response.data.data.user.financialDetails ||
            //       !response.data.data.user.financialDetails.accountNumber)
            //   ) {
            //     router.push("/clinic/setup-bank-detail");
            //   } else if(response.data.data.user.userType === "clinic" && response.data.data.user.defaultPaymentMethod ==='CreditCard' &&
            //     (!response.data.data.user.paymentDetails ||
            //       !response.data.data.user.paymentDetails.cardId)
            //   ){
            //     router.push("/clinic/setup-bank-detail");
            //   }
            //   else {
            //     router.push("/clinic/dashboard");
            //   }
            // }
            if (response.data.data.user.userType === "doctor") {
              router.push("/doctor/dashboard");
            }

            if (response.data.data.user.userType === "nurse") {
              router.push("/nurse/dashboard");
            }
            if (response.data.data.user.userType === "admin") {
              router.push("/admin/dashboard");
            }

            // if (
            //   response.data.data.user.userType === "clinic" &&
            //   !response.data.data.user.isProfileCompleted
            // ) {
            //   router.push("/clinic-complete-profile");
            // }

            // if (
            //   response.data.data.user.userType === "franchise" &&
            //   !response.data.data.user.isProfileCompleted
            // ) {
            //   router.push("/franchise-complete-profile");
            // }

            resolve(response);
          } else {
            reject({message: response.data.message});
          }
        })
        .catch(response => {
          if (
            response &&
            response.message &&
            response.message === "Request failed with status code 401"
          )
            reject({
              message: "Email/Password is invalid. Please try again."
            });
          else if (!response.data) {
            reject({
              message: response
            });
          } else reject({message: response.data.message});
        });
    });
  },
}
